/// <reference types="Cypress" />

context('Merchants', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('should add a new merchant', () => {
    const firstname = 'John';
    const lastname = 'Doe';
    const email = 'j.doe@gmail.com';
    const phone = '+1 929 245 90 97';

    cy.get('[data-locator=add-merchant-button]')
      .click()
      .should('be', 'enabled');

    cy.get('button[type=submit]').should('be', 'disabled');

    cy.get('input[name=firstname]').type(firstname);

    cy.get('input[name=lastname]').type(lastname);

    cy.get('input[name=email]').type(email);

    cy.get('input[name=phone]').type(phone);

    cy.contains('Has Premium').click();

    cy.get('button[type=submit]')
      .click()
      .should('be', 'enabled');

    cy.get('[data-locator=merchants-table-row]')
      .should('contain', firstname)
      .should('contain', lastname)
      .should('contain', email)
      .should('contain', phone);
  });
});
