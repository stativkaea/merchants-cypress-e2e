/// <reference types="Cypress" />

context('Merchants', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('should edit an existing merchant', () => {
    const phone = '+1 929 245 90 97';

    cy.get('[data-locator=edit-merchant-button]')
      .last()
      .click()
      .should('be', 'enabled');

    cy.get('button[type=submit]').should('be', 'disabled');

    cy.get('input[name=phone]')
      .clear()
      .type(phone)
      .blur();

    cy.get('button[type=submit]')
      .click()
      .should('be', 'enabled');

    cy.get('[data-locator=merchants-table-row]').should('contain', phone);
  });
});
