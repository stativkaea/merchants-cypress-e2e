/// <reference types="Cypress" />

context('Merchants', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('should remove an existing merchant', () => {
    const email = cy
      .get('[data-locator=merchants-table-row-email]')
      .first()
      .invoke('text');

    cy.get('[data-locator=remove-merchant-button]')
      .first()
      .click()
      .should('be', 'enabled');

    cy.contains('Yes').click();

    cy.get('[data-locator=merchants-table-row-email]').should('not.contain', email);
  });
});
